//
//  Singleton.m
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

+(Singleton *)sharedInstance{
    static Singleton *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[Singleton alloc]init];
    });
    
    
    return _sharedInstance;
}

@end
