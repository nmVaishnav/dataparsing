//
//  Singleton.h
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : NSUInteger {
    
    APITypeGetOriginalData = 0, //We're Utilizing only this one
    APITypeGetSearchResults,
    APITypeGetDistanceData
    
} APIType;

@interface Singleton : NSObject

+(Singleton *)sharedInstance;

@end
