//
//  ParseManager.h
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Singleton.h"
#import "Constants.h"

@class ParseManager;
@protocol ParseManagerDelegate <NSObject>

@required
//Required Protocols

-(void) requestCompletedWithAPIType : (APIType)apiType dictionaryResponse:(NSDictionary *)dictionaryResponse error:(NSError *)error;

@optional
//Optional Protocols

@end

@interface ParseManager : NSObject
@property(nonatomic,strong) id<ParseManagerDelegate> delegates;
@property(nonatomic,readwrite)APIType apiType;

- (id) initWithDelegate : (id)delegate withAPIType:(APIType)apiTypeSelected;
- (NSString *)description;
- (void) callWebService;

@end

