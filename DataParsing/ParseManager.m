//
//  ParseManager.m
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import "ParseManager.h"

@implementation ParseManager{
    NSURL *jsonURL;
}
@synthesize apiType;
@synthesize delegates;

-(id)initWithDelegate:(id)delegate withAPIType:(APIType)apiTypeSelected{
    self = [super init];
    if (self) {
        self.delegates = delegate; // expecting a Controller
        apiType = apiTypeSelected;
    }
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"Object Description : %lu \n URL : %@",apiType,jsonURL];
}
-(void)callWebService{

    
    jsonURL = [self getURL];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:jsonURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        
        NSDictionary *dictJSONResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (error) {
            // if Errore Occure
            [delegates requestCompletedWithAPIType:apiType dictionaryResponse:nil error:error];
        }else{
            // No Error
            [delegates requestCompletedWithAPIType:apiType dictionaryResponse:dictJSONResponse error:nil];
        }
        
    }];
    [dataTask resume];
    
    
}

-(NSURL *)getURL{
    switch (apiType) {
        case APITypeGetOriginalData:
            return [NSURL URLWithString:DATA_URL];
            break;
        case APITypeGetDistanceData:{
            
            /// If There s an Another Attributes to pass into Api  . . .(ex. Pagging .. . )
            /*
             NSUInteger page;
             
             if ([delegates isKindOfClass:[ViewController class]]) {
             
             page = [(ViewController *)delegates page];
             
             }else{
             
             page = 1;
             
             }
             NSString *str = [NSString stringWithFormat:APIWithParametere,page];
             
             
             */
            //             return [NSURL URLWithString:str];
            return [NSURL URLWithString:@"Distance URL"];
        }
            break;
        case APITypeGetSearchResults:
            return [NSURL URLWithString:@"Search URL"];
            break;
            
    }
}
@end
