//
//  ObjectFile.m
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import "ObjectFile.h"

@implementation ObjectFile
@synthesize strTitle;
@synthesize strAuthor;
@synthesize strImageURL;


-(NSString *)description{
    
    NSString *descriptionString = [NSString stringWithFormat:@"Title : %@ \r author: %@ \r ImageURL : %@ \r",strTitle,strAuthor,strImageURL];
    
    return descriptionString;
}

@end
