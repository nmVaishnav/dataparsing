//
//  ObjectFile.h
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface ObjectFile : NSObject


@property (nonatomic,strong) NSString *strTitle;
@property (nonatomic,strong) NSString *strAuthor;
@property (nonatomic,strong) NSString *strImageURL;


@end
