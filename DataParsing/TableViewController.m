//
//  TableViewController.m
//  DataParsing
//
//  Created by Naman on 12/28/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import "TableViewController.h"
#import "ParseManager.h"
#import "ObjectFile.h"
@interface TableViewController ()<ParseManagerDelegate>{
    NSMutableArray *arrDataStore;
    
}

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ParseManager *obj = [[ParseManager alloc]initWithDelegate:self withAPIType:APITypeGetOriginalData];
    [obj callWebService];
    
    arrDataStore = [[NSMutableArray alloc]init];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

// Delegate Implementation
- (void)requestCompletedWithAPIType:(APIType)apiType dictionaryResponse:(NSDictionary *)dictionaryResponse error:(NSError *)error{
    
    NSLog(@"Dictionary %@", dictionaryResponse);
    
    NSArray *accessArray = [dictionaryResponse objectForKey:@"posts"];
    
    NSLog(@"%@",accessArray);
    
    for (NSDictionary *dict in accessArray) {
        
        ObjectFile *obj = [[ObjectFile alloc]init];
        
        obj.strTitle = [dict objectForKey:@"title"];
        obj.strAuthor = [dict objectForKey:@"author"];
        obj.strImageURL = [dict objectForKey:@"thumbnail"];
        
        [arrDataStore addObject:obj];
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.tableView reloadData];
    });
    
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrDataStore.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    ObjectFile *obj = [arrDataStore objectAtIndex:indexPath.row];
    cell.textLabel.text = obj.strTitle;
    // Configure the cell...
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
